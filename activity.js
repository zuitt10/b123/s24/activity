//find users with letter 'a' in their firstName or lastName
	//show only their email and isAdmin properties/fields
db.users.find({$or:[{firstName:{$regex: 'a'}},{lastName:'a'}]}, {_id:0,email:1,isAdmin:1})

//find users with letter 'i' in their firstName and  is an admin.
	//show only their email and isAdmin properties/fields
	db.users.find({$and:[{firstName:{$regex: 'a'}},{isAdmin:true}]}, {_id:0,email:1,isAdmin:1})

//find products with letter x in its name and has a price greater than or equal to 50000
db.products.find({$and:[{name: {$regex:'x'}},{price:{$gte:50000}}]})

//update products price with less than 1000 to inactive
db.products.updateMany({price:{$lt:1000}},{$set:{isActive:false}})

//delete users with  tech in their email
db.users.deleteMany({email:{$regex: 'tech',$options: '$i'}})